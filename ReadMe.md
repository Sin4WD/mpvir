## what is this?
This is an api server for wrapping libmpv inside arm embedded boards
## Requirements
- [x] play video
- [x] control video
    - [x] pause/unpause
    - [x] mute/unmute
    - [x] seek (all kinds)
    - [x] get playback time
    - [x] volume
- [ ] capture wifi credentials and connect
    - [x] web page
    - [ ] api
    - [ ] connect
- [x] dump
- [x] systemd service

## tools & lib
- [x] mongoose
- [x] sqlite
- [x] libmpv
- [x] makefile
- [x] debug tools
    - [x] debug main
    - [x] dump
    - [x] log
- [x] unittest
- [x] doc
- [ ] log4c or slog


## prerequisites
```
sudo apt-get install mpv libmpv-dev
```
## run
```
make
make run
make install
```
## debug
```
tail -f .tmp/dump
make debug_main
```
## example
refer to api documentation