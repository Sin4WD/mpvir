DIR := ${CURDIR}
PROG = api_server
DBG_PROG = api_server_dbg
UNIT_DEBUG_MAIN = debug_main
SOURCES = lib/db_plugin/sqlite3.c lib/db_plugin/db_plugin_sqlite.c lib/mongoose/mongoose.c lib/mpv_interface/mpvi.c
CFLAGS = -W -Wall -ldl -lm -pthread -lmpv -DNDEBUG $(CFLAGS_EXTRA)
CFLAGS_2 = -Wimplicit-fallthrough=0 
COMMON_CMD= $(DIR)/$(PROG) -f $(DIR)/.tmp/$(PROG).db -demo $(DIR)/sample_video.mp4
ARM_CMD = startx $(COMMON_CMD) -mpvc $(DIR)/lib/mpv_interface/config_r69  -sidetty 7
x86_CMD = $(COMMON_CMD) -r web_root -mpvc $(DIR)/lib/mpv_interface/config  -sidetty 3
ARCH := $(shell uname -p)

ifeq ($(ARCH),x86_64)
	CMD = $(x86_CMD)
	CFLAGS += $(CFLAGS_2)
else
	CMD = $(ARM_CMD)
endif

all: $(PROG)
	mkdir -p .tmp

$(PROG): $(SOURCES) $(PROG).c
	$(CC) $(SOURCES) $(PROG).c -o $@ $(CFLAGS)

$(DBG_PROG): $(SOURCES) $(DBG_PROG).c
	$(CC) $(SOURCES) $(PROG).c -o $@ $(CFLAGS) -g

$(UNIT_DEBUG_MAIN): $(SOURCES) $(UNIT_DEBUG_MAIN).c
	$(CC) $(SOURCES) $(UNIT_DEBUG_MAIN).c -o $@ $(CFLAGS) -g

test: $(PROG)
	sh unit_test.sh $(DIR)/$(PROG)

echo_cmd:
	@echo "$(CMD)"

run: all
	$(CMD)

.tmp/run.sh: .tmp
	echo "#!/bin/sh" > .tmp/run.sh
	echo $(CMD) >> .tmp/run.sh

install: .tmp/run.sh all
	mkdir -p /var/local/mpvi
	cp .tmp/run.sh /var/local/mpvi/run.sh
	chmod +x /var/local/mpvi/run.sh
	cp service/mpvi.service /etc/systemd/system/
	systemctl daemon-reload
	systemctl start mpvi.service
	systemctl enable mpvi.service

clean:
	rm -rf *.gc* *.dSYM *.exe *.obj *.o a.out $(PROG) $(DBG_PROG) .tmp/run.sh
