#!/bin/sh

PROG=$1
PORT=${2:-8000}  # If second param is given, this is load balancer port
DB_FILE=/tmp/_$$.db
URL=http://127.0.0.1:$PORT/api/v1

cleanup() {
  rm -rf $DB_FILE
  kill -9 $PID >/dev/null 2>&1
}
echo $PROG
echo $DB_FILE
#set -x
trap cleanup EXIT

cleanup
make run &
PID=$!

## todo

sleep 2
echo ">>>>>>>>>>>>>>>> Opening"
curl -s -X POST http://127.0.0.1:8000/api/v1/vid -d "https://hw17.cdn.asset.aparat.com/aparat-video/4e0d8521bd5293fe63a115d57536699824628288-144p.mp4" > /dev/null
sleep 10
curl -s http://127.0.0.1:8000/api/v1/vid/cmd/playback-time
echo ">>>>>>>>>>>>>>>> seek forward"
curl -s -X PUT http://127.0.0.1:8000/api/v1/vid/cmd/seek -d "10&relative+exact" > /dev/null
curl -s http://127.0.0.1:8000/api/v1/vid/cmd/playback-time 
sleep 1
echo "|||||||||||||||| pause"
curl -s http://127.0.0.1:8000/api/v1/vid/cmd/pause > /dev/null
curl -s -X PUT http://127.0.0.1:8000/api/v1/vid/cmd/seek -d "-10&relative" > /dev/null
sleep 2
echo ">>>>>>>>>>>>>>>> unpause"
curl -s http://127.0.0.1:8000/api/v1/vid/cmd/unpause > /dev/null
sleep 1
echo ">>>>>>>>>>>>>>>> mute"
curl -s http://127.0.0.1:8000/api/v1/vid/cmd/mute > /dev/null
sleep 5
echo ">>>>>>>>>>>>>>>> unmute"
curl -s http://127.0.0.1:8000/api/v1/vid/cmd/unmute > /dev/null
sleep 5
curl -s -X POST http://127.0.0.1:8000/api/v1/vid -d "sample_video.mp4" > /dev/null
sleep 10
curl -s -X DELETE http://127.0.0.1:8000/api/v1/vid > /dev/null
sleep 2
# Fetch existing key
# RESULT=$(curl -s $URL/bar/baz)
# test "$RESULT" = "success" || exit 1

# Delete it
# curl -s -X DELETE $URL/bar/baz

# Make sure it's deleted - GET must result in 404
# RESULT=$(curl -s -i $URL/bar/baz | head -1 | tr -d '\r')
# test "$RESULT" = "HTTP/1.1 404 Not Found" || exit 1

exit 0
