#ifndef mpv_INTERFACE_H
#define mpv_INTERFACE_H
#include<inttypes.h>
#define MPV_VIEW_READY 2
#define MPV_VIEW_IDLE 1

int mpvi_client_be_created_and_configured(const char * mpv_config_dir);
int mpvi_force_close_init(void);

int mpvi_play_url(const char * url);
int mpvi_pause(int pause);
int mpvi_mute(int mute);
int mpvi_change_volume(const int * delta_volume, int * result_volume);
int mpvi_seek(const char * seconds, const char* type);
int mpvi_quit(void);
int mpvi_ptime(int64_t * ptime);

int mpvi_poll_events(unsigned int milis);
int mpvi_wait_blocking(void);
int mpvi_terminate_blocking(void);


#endif // mpvi_INTERFACE_H