#include "mpvi.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpv/client.h>

static mpv_handle *mpv_ctx;

/** mpvi_state
 *  0 : clear to go
 * -1 : not init
 *  1 : waiting for *waiting_for
 */ 
static int mpvi_state = -1; // todo make this thread-safe
static mpv_event_id waiting_for = 0;
static char * local_mpv_config_dir;

static inline int check_error(int status)
{
    if (status < 0) {
        printf("mpv API error: %s\n", mpv_error_string(status));
    }
    return status;
}

static inline int haserr(int return_code){
    return return_code;
}

static inline int mpvi_wait_for(mpv_event_id event_id){
    if(mpvi_state) return 1;
    waiting_for = event_id;
    mpvi_state = 1;
    return 0;
}

int mpvi_client_be_created_and_configured(const char * mpv_config_dir){
    if(mpv_config_dir) local_mpv_config_dir = (char*) mpv_config_dir;
    if(!local_mpv_config_dir) return 1;
    if(!mpv_ctx){
        mpv_ctx = mpv_create();
        if (!mpv_ctx) {
            printf("failed creating context\n");
            return 1;
        }
        mpv_set_option_string(mpv_ctx, "config-dir", local_mpv_config_dir); // set config root
        mpv_set_option_string(mpv_ctx, "config", "yes"); 
    }
    return 0;
}

int _mpv_initialize(){
    check_error(mpv_initialize(mpv_ctx));
    mpvi_state = 0;
    return 0;
}

int _mpv_client_be_initialized(const char * mpv_config_dir, int lazy){
    int return_code = 0;
    if(haserr(mpvi_client_be_created_and_configured(mpv_config_dir))) return 1;
    if(!lazy){
        if(mpvi_state<0)return_code = _mpv_initialize();
    }
    return return_code;
}

int mpvi_force_close_init(){
    if(mpv_ctx){
        mpvi_terminate_blocking();
    }
    return _mpv_client_be_initialized(NULL,0);
}

int _mpvi_client_be_ready_to_play(){
    if(waiting_for==MPV_EVENT_FILE_LOADED){
        printf("[%ld] %d can't load new file waiting for loadfile\n",time(NULL), mpvi_state);
        return 1;
    }
    if(haserr(_mpv_client_be_initialized(NULL,0))){
        return mpvi_force_close_init();
    }
    return 0;
}

static inline int _mpvi_client_be_ready_for_cmd(){
    return mpvi_state;
}
// command //
int mpvi_play_url(const char * url){
    printf("[%ld] %d req4 loadfile\n",time(NULL), mpvi_state);
    if(haserr(_mpvi_client_be_ready_to_play())) return 1;
    // this function blocks others until file is loaded successfuly
    if(haserr(mpvi_wait_for(MPV_EVENT_FILE_LOADED))) return 1;
    const char* cmd_loadfile = "loadfile";
    const char *cmd[] = {cmd_loadfile, url, NULL};
    check_error(mpv_command(mpv_ctx, cmd));
    return 0;
}

int mpvi_pause(int pause){
    printf("[%ld] %d req4 %spause\n",time(NULL), mpvi_state,pause?"":"un");
    if(haserr(_mpvi_client_be_ready_for_cmd())) return 1;
    pause = pause?1:0;
    return check_error(mpv_set_property(mpv_ctx, "pause", MPV_FORMAT_FLAG,(void *) &pause));
}

int mpvi_mute(int mute){
    printf("[%ld] %d req4 %smute\n",time(NULL), mpvi_state,mute?"":"un");
    if(haserr(_mpvi_client_be_ready_for_cmd())) return 1;
    // ao-mute is not supported yet
    // mute = mute?1:0;
    // return check_error(mpv_set_property(mpv_ctx, "ao-mute", MPV_FORMAT_FLAG,(void *) &mute));
    double volume = mute?0.0:100.0;
    return check_error(mpv_set_property(mpv_ctx, "volume", MPV_FORMAT_DOUBLE,(void *) &volume));   
}

int mpvi_change_volume(const int * delta_volume, int * result_volume){
    printf("[%ld] %d req4 volume%s%d\n",time(NULL), mpvi_state,delta_volume>0?"+":"-",*delta_volume);
    if(haserr(_mpvi_client_be_ready_for_cmd())) return 1;
    double volume;
    check_error(mpv_get_property(mpv_ctx,"volume", MPV_FORMAT_DOUBLE, &volume));
    volume += (double) *delta_volume;
    volume = volume>100?100.0:(volume<0?0.0:volume);
    *result_volume = (int) volume;
    return check_error(mpv_set_property(mpv_ctx, "volume", MPV_FORMAT_DOUBLE,(void *) &volume));   
}

int mpvi_seek(const char * seconds, const char* type){
    printf("[%ld] %d req4 seek\n",time(NULL), mpvi_state);
    if(haserr(_mpvi_client_be_ready_for_cmd())) return 1;
    const char* cmd_seek = "seek";
    const char *cmd[] = {cmd_seek, seconds, type, NULL};
    check_error(mpv_command(mpv_ctx, cmd));
    return 0;
}

int mpvi_ptime(int64_t * ptime){
    printf("[%ld] %d req4 ptime\n",time(NULL), mpvi_state);
    if(haserr(_mpvi_client_be_ready_for_cmd())) return 1;
    return check_error(mpv_get_property(mpv_ctx,"playback-time", MPV_FORMAT_INT64, ptime));
}
/////////////
void _safely_shutdown(int blocking){
    waiting_for = 0;
    mpvi_state = -1;
    if(blocking){
        mpv_terminate_destroy(mpv_ctx);
    }else{
        mpv_detach_destroy(mpv_ctx);
    }
    mpv_ctx=NULL;
}

int mpvi_poll_events(unsigned int milis){
    if(!mpv_ctx || mpvi_state<0){
        return 0;
    }
    mpv_event *event;
    event = mpv_wait_event(mpv_ctx, milis/1000.0);
    if (event->event_id == MPV_EVENT_NONE){
        return 0;
    } else {
        printf("[%ld] mpv_event %d: %s\n", time(NULL), event->event_id, mpv_event_name(event->event_id));
        if (event->event_id == waiting_for){
            waiting_for = 0;
            mpvi_state = 0;
        }
        if(event->event_id == MPV_EVENT_END_FILE || event->event_id == MPV_EVENT_IDLE){
            return MPV_VIEW_IDLE;
        }
        if(event->event_id == MPV_EVENT_FILE_LOADED){
            return MPV_VIEW_READY;
        }
        if (event->event_id == MPV_EVENT_SHUTDOWN){
            _safely_shutdown(1);
            return MPV_VIEW_IDLE;
        }
    }
    return 0;
}

int mpvi_wait_blocking(){
    while (1) {
        mpv_event *event = mpv_wait_event(mpv_ctx, 10000);
        printf("[%ld] mpv_event %d: %s\n", time(NULL), event->event_id, mpv_event_name(event->event_id));
        if (event->event_id == MPV_EVENT_SHUTDOWN)
            break;
    }

    _safely_shutdown(1);
    return 0;
}
int mpvi_terminate_blocking(){
    _safely_shutdown(1);
    if(haserr(mpvi_client_be_created_and_configured(NULL))) return 1;
    return 0;
}
