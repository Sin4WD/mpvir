#include "lib/db_plugin/db_plugin.h"
#include "lib/mpv_interface/mpvi.h"
#include <stdlib.h>

static const char *s_http_port = "8000";
static struct mg_serve_http_opts s_http_server_opts;
static int s_sig_num = 0;
static void *s_db_handle = NULL;
static const char *s_db_path = "api_server.db";
static const char *s_mpv_config_path = "~/.config/mpv/mpv.conf";
static const char *s_sample_video = "sample_video.mp4";
static const char *s_sidecar_tty;
static const char *s_self_tty;
static const struct mg_str s_get_method = MG_MK_STR("GET");
static const struct mg_str s_put_method = MG_MK_STR("PUT");
static const struct mg_str s_post_method = MG_MK_STR("POST");
static const struct mg_str s_delete_method = MG_MK_STR("DELETE");


static void switch_tty(const char * vtn){
    if(!s_sidecar_tty){
      return;
    }
    char cmd[12]={0};
    sprintf(cmd,"chvt %s;",vtn);
    printf("Switching tty -> %s\n", cmd);
    system(cmd);
}

static void signal_handler(int sig_num) {
  signal(sig_num, signal_handler);
  switch_tty(s_sidecar_tty);
  s_sig_num = sig_num;
}

static int has_prefix(const struct mg_str *uri, const struct mg_str *prefix) {
  return uri->len >= prefix->len && memcmp(uri->p, prefix->p, prefix->len) == 0;
}

static int is_equal(const struct mg_str *s1, const struct mg_str *s2) {
  return s1->len == s2->len && memcmp(s1->p, s2->p, s2->len) == 0;
}

static void mg_str_dup2_str(char ** dest, struct mg_str * src){
  *dest = (char *) malloc(src->len + 1);
  if (*dest != NULL) {
    memcpy(*dest, src->p, src->len);
    (*dest)[src->len] = '\0';
  }
}

static void ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
  static const struct mg_str api_prefix = MG_MK_STR("/api/v1");
  static const struct mg_str wifi_prefix = MG_MK_STR("/wlan");
  static const struct mg_str kill_prefix = MG_MK_STR("/kill");
  static const struct mg_str store_prefix = MG_MK_STR("/store");
  static const struct mg_str vid_prefix = MG_MK_STR("/vid");
  static const struct mg_str vid_demo = MG_MK_STR("/demo");
  static const struct mg_str vid_close = MG_MK_STR("/close");
  static const struct mg_str vid_cmd_prefix = MG_MK_STR("/cmd");
  static const struct mg_str vid_cmd_seek = MG_MK_STR("/seek");
  static const struct mg_str vid_cmd_pause = MG_MK_STR("/pause");
  static const struct mg_str vid_cmd_unpause = MG_MK_STR("/unpause");
  static const struct mg_str vid_cmd_mute = MG_MK_STR("/mute");
  static const struct mg_str vid_cmd_unmute = MG_MK_STR("/unmute");
  static const struct mg_str vid_cmd_change_volume = MG_MK_STR("/change-volume");
  static const struct mg_str vid_cmd_ptime = MG_MK_STR("/playback-time");


  struct http_message *hm = (struct http_message *) ev_data;
  struct mg_str path_ptr;

  switch (ev) {
    case MG_EV_ACCEPT: {
      char addr[32];
      mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
                          MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
      printf("%p: Connection from %s\r\n", nc, addr);
      break;
    }
    case MG_EV_CLOSE: {
      char addr[32];  
      mg_sock_addr_to_str(&nc->sa, addr, sizeof(addr),
                          MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
      printf("%p: Connection closed %s\r\n", nc, addr);
      break;
    }
    case MG_EV_HTTP_REQUEST:
      printf("HTTP request on %.*s\r\n",(int)(hm->uri.len), hm->uri.p);
      if (has_prefix(&hm->uri, &api_prefix)) { // /api/v1
        char *body;
        path_ptr.p = hm->uri.p + api_prefix.len;
        path_ptr.len = hm->uri.len - api_prefix.len;
        if (has_prefix(&path_ptr, &wifi_prefix)){ // /api/v1/wlan
          // todo : raises 501 Not Implemented
          mg_printf(nc, "%s",
            "HTTP/1.0 501 Not Implemented\r\n"
            "Content-Length: 0\r\n\r\n");
        } else if (has_prefix(&path_ptr, &kill_prefix)){ // /api/v1/kill
          mg_send_response_line(nc,200,NULL);
          raise(SIGTERM);
        } else if (has_prefix(&path_ptr, &vid_prefix)){ // /api/v1/vid
          mg_str_dup2_str(&body,&hm->body); // must free body at the end
          if (is_equal(&path_ptr, &vid_prefix)){
            if(is_equal(&hm->method, &s_post_method)) {
              mpvi_play_url(body);
              mg_send_response_line(nc,200,NULL);
            }else if(is_equal(&hm->method, &s_delete_method)){
              mpvi_terminate_blocking();
              mg_send_response_line(nc,200,NULL);
            }
          } else {
            path_ptr.p = path_ptr.p + vid_prefix.len;
            path_ptr.len = path_ptr.len - vid_prefix.len;
            if (has_prefix(&path_ptr, &vid_cmd_prefix)){ // /api/v1/vid/cmd
              path_ptr.p = path_ptr.p + vid_cmd_prefix.len;
              path_ptr.len = path_ptr.len - vid_cmd_prefix.len;
              if (is_equal(&path_ptr, &vid_cmd_seek)){ // /api/v1/vid/cmd/seek -d "10&relative"
                char * type = strchr(body, '&');
                *type='\0';
                mg_send_response_line(nc,mpvi_seek(body,++type)?400:200,NULL);
              } else if (is_equal(&path_ptr, &vid_cmd_pause)){ // /api/v1/vid/cmd/pause
                mg_send_response_line(nc,mpvi_pause(1)?400:200,NULL);
              } else if (is_equal(&path_ptr, &vid_cmd_unpause)){ // /api/v1/vid/cmd/unpause
                mg_send_response_line(nc,mpvi_pause(0)?400:200,NULL);
              } else if (is_equal(&path_ptr, &vid_cmd_mute)){ // /api/v1/vid/cmd/mute
                mg_send_response_line(nc,mpvi_mute(1)?400:200,NULL);  
              } else if (is_equal(&path_ptr, &vid_cmd_unmute)){ // /api/v1/vid/cmd/unmute
                mg_send_response_line(nc,mpvi_mute(0)?400:200,NULL);
              } else if (is_equal(&path_ptr, &vid_cmd_change_volume)){ // /api/v1/vid/cmd/change-volume
                int volume_change = atoi(body);
                int result_volume,temp,content_lenght = {0};
                int code = mpvi_change_volume(&volume_change,&result_volume);
                mg_send_response_line(nc,code?400:200,NULL);
                for(temp=result_volume;temp!=0;content_lenght++, temp /= 10);
                mg_send_head(nc,200,content_lenght,"Content-Type: text/plain");
                mg_printf(nc,"%d", result_volume);
              } else if (is_equal(&path_ptr, &vid_cmd_ptime)){ // /api/v1/vid/cmd/playback-time
                int64_t ptime = -1;
                int64_t temp = 0;
                int64_t content_lenght = 0;
                mpvi_ptime(&ptime);
                for(temp=ptime;temp!=0;content_lenght++, temp /= 10);
                mg_send_head(nc,200,content_lenght,"Content-Type: text/plain");
                mg_printf(nc,"%lld", ptime);
              }
            } else if (is_equal(&path_ptr, &vid_demo)){ // /api/v1/vid/demo
              mpvi_play_url(s_sample_video);
              mg_send_response_line(nc,200,NULL);
            } else if (is_equal(&path_ptr, &vid_close)){ // /api/v1/vid/close
              int result=0;
              result = mpvi_terminate_blocking();
              mg_send_response_line(nc,result?400:200,NULL);
            }
          }
          nc->flags |= MG_F_SEND_AND_CLOSE;
          free(body);
          break;
        } else if(has_prefix(&path_ptr, &store_prefix)){ // key value db /api/v1/store
          path_ptr.p = path_ptr.p + store_prefix.len;
          path_ptr.len = path_ptr.len - store_prefix.len;
          if (is_equal(&hm->method, &s_get_method)) {
            db_op(nc, hm, &path_ptr, s_db_handle, API_OP_GET);
          } else if (is_equal(&hm->method, &s_put_method)) {
            db_op(nc, hm, &path_ptr, s_db_handle, API_OP_SET);
          } else if (is_equal(&hm->method, &s_delete_method)) {
            db_op(nc, hm, &path_ptr, s_db_handle, API_OP_DEL);
          }
        }
        mg_printf(nc, "%s",
            "HTTP/1.0 501 Not Implemented\r\n"
            "Content-Length: 0\r\n\r\n");
      } else {
        mg_serve_http(nc, hm, s_http_server_opts); /* Serve static content */
      }
      break;
    default:
      break;
  }
}

static void cleanup_server(struct mg_mgr* mgrp){
  /* Cleanup */
  mg_mgr_free(mgrp);
  db_close(&s_db_handle);
  mpvi_terminate_blocking();
  printf("Exiting on signal %d\n", s_sig_num);
}


static void run_server(struct mg_mgr* mgrp){
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  /* Run event loop until signal is received */
  printf("Starting RESTful server on port %s\n", s_http_port);
  int result = 0;
  switch_tty(s_sidecar_tty);
  while (s_sig_num == 0) {
    result = 0;
    mg_mgr_poll(mgrp, 10);
    
    result = mpvi_poll_events(10);

    if(result==MPV_VIEW_IDLE){
      switch_tty(s_sidecar_tty);
    }else if (result==MPV_VIEW_READY){
      switch_tty(s_self_tty);
    }
  }
  switch_tty(s_sidecar_tty);
}

static void init_server(struct mg_mgr* mgrp, struct mg_connection ** ncp){
  /* Open listening socket */
  
  *ncp = mg_bind(mgrp, s_http_port, ev_handler);
  mg_set_protocol_http_websocket(*ncp);
  
    /* Open database */
  if ((s_db_handle = db_open(s_db_path)) == NULL) {
     fprintf(stderr, "Cannot open DB [%s]\n", s_db_path);
    exit(EXIT_FAILURE);
  }

  /* Init MPV */
  mpvi_client_be_created_and_configured(s_mpv_config_path);
}

int main(int argc, char *argv[]) {
  struct mg_mgr mgr;
  struct mg_connection *nc;
  int i;
  s_self_tty = getenv("XDG_VTNR");
  printf("api server loaded on tty %s\n",s_self_tty);
  mg_mgr_init(&mgr, NULL);
  s_http_server_opts.document_root = "web_root";
  /* Parse command line arguments */
  for (i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-D") == 0) {
      mgr.hexdump_file = argv[++i];
    } else if (strcmp(argv[i], "-f") == 0) {
      s_db_path = argv[++i];
    } else if (strcmp(argv[i], "-r") == 0) {
      s_http_server_opts.document_root = argv[++i];
    } else if (strcmp(argv[i], "-mpvc") == 0) {
      s_mpv_config_path = argv[++i];
    }else if (strcmp(argv[i], "-demo") == 0) {
      s_sample_video = argv[++i];
    }else if (strcmp(argv[i], "-sidetty") == 0) {
      s_sidecar_tty = argv[++i];
    }
  }
  printf("sidecar tty is %s\n",s_sidecar_tty?s_sidecar_tty:"not set.");

  init_server(&mgr,&nc);
  run_server(&mgr);
  cleanup_server(&mgr);
  return 0;
}
