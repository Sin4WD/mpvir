## play_video
- description: plays the given video.
- method: `POST`
- synopsis: `curl -v -X POST <ip:port>/api/v1/vid -d <url>`
- sample: `curl -v -X POST http://127.0.0.1:8000/api/v1/vid -d "https://hw17.cdn.asset.aparat.com/aparat-video/4e0d8521bd5293fe63a115d57536699824628288-144p.mp4"`
- response:
    - 200: *command received.
    - 400: there was a problem opening the file.
## close mpv
- description: closes the mpv player
- method: `DELETE`
- synopsis: `curl -v -X DELETE <ip:port>/api/v1/vid`
- sample: `curl -v -X DELETE http://127.0.0.1:8000/api/v1/vid`
- response:
    - 200: command executed.
## close mpv (alterntive)
- description: closes the mpv player and return 400 if reconfiguration was failed
- method: all
- synopsis: `curl <ip:port>/api/v1/vid/close`
- sample: `curl http://127.0.0.1:8000/api/v1/vid/close`
- response:
    - 200: command executed.
    - 400: closed but there was a problem reconfiguring the mpv.
## play demo video
- description: plays a demo video.
- method: `POST`
- synopsis: `curl <ip:port>/api/v1/vid/demo`
- sample: `curl http://127.0.0.1:8000/api/v1/vid/demo`
- response:
    - 200: *command received.
    - 400: there was a problem opening the file.
## kill all
- description: kills the service no matter what. (Hard reset!)
- method: all
- synopsis: `curl -v <ip:port>/api/v1/kill`
- sample: `curl -v http://127.0.0.1:8000/api/v1/kill`
- response:
    - Connection Closed!
## pause
- description: pauses the video.
- method: all
- synopsis: `curl -v <ip:port>/api/v1/vid/cmd/pause`
- sample: `curl -v http://127.0.0.1:8000/api/v1/vid/cmd/pause`
- response:
    - 200: command executed.
    - 400: no video playing or there was a problem executing command.
## unpause
- description: unpauses the video.
- method: all
- synopsis: `curl -v <ip:port>/api/v1/vid/cmd/unpause`
- sample: `curl -v http://127.0.0.1:8000/api/v1/vid/cmd/unpause`
- response:
    - 200: command executed.
    - 400: no video playing or there was a problem executing command.
## mute
- description: mutes the audio output.
- method: all
- synopsis: `curl -v <ip:port>/api/v1/vid/cmd/mute`
- sample: `curl -v http://127.0.0.1:8000/api/v1/vid/cmd/mute`
- response:
    - 200: command executed.
    - 400: no video playing or there was a problem executing command.
## unmute
- description: unmutes the audio output.
- method: all
- synopsis: `curl -v <ip:port>/api/v1/vid/cmd/unmute`
- sample: `curl -v http://127.0.0.1:8000/api/v1/vid/cmd/unmute`
- response:
    - 200: command executed.
    - 400: no video playing or there was a problem executing command.
## seek
- description: unpauses the video.
- method: `PUT`
- synopsis: `curl -v -X PUT <ip:port>/api/v1/vid/cmd/change-volume -d "<delta_volume:int>"`
- sample: `curl -v -X PUT http://127.0.0.1:8000/api/v1/vid/cmd/change-volume -d "-10"`
- args:
    - delta_volume `int`: positive or negative volume change in percent
- response:
    - 200: command executed.
    - 400: no video playing or there was a problem executing command.
- response-body:
    - current volume after change
    - plain text int
    - sample: `50` means volume is 50 percent
## seek
- description: unpauses the video.
- method: `PUT`
- synopsis: `curl -v -X PUT <ip:port>/api/v1/vid/cmd/seek -d "<seconds>&<type>"`
- sample: `curl -v -X PUT http://127.0.0.1:8000/api/v1/vid/cmd/seek -d "-10&relative+exact"`
- response:
    - 200: command executed.
    - 400: no video playing or there was a problem executing command.
- args:
    - seconds `int`: positive or negative/ absolute or relative integer in seconds or in percent
    - type `str`:
        - Options:
            -`relative` (default): Seek relative to current position (a negative value seeks backwards).
            - `absolute`: Seek to a given time (a negative value starts from the end of the file).
            - `absolute-percent`: Seek to a given percent position.
            - `relative-percent`: Seek relative to current position in percent.
            - `keyframes`: Always restart playback at keyframe boundaries (fast).
            - `exact`: Always do exact/hr/precise seeks (slow).
        > * Multiple flags can be combined, e.g.: absolute+keyframes.
        > * By default, keyframes is used for relative, relative-percent, and absolute-percent seeks, while exact is used for absolute seeks.
                
## playback-time
- description: returns current playback time of video in seconds.
- method: `GET`
- synopsis: `curl -v <ip:port>/api/v1/vid/cmd/playback-time"`
- sample: `curl -v http://127.0.0.1:8000/api/v1/vid/cmd/playback-time"`
- response:
    - 200: command executed and content returned.
    - 400: no video playing or there was a problem executing command.
- response body:
    - plain text int64
    - sample: `24` means video is at 24th second.