## store
- example:
    - `curl -X PUT -d 'value=123' http://127.0.0.1/api/v1/store/foo`
    - `curl -s -X PUT -d 'value=success' http://127.0.0.1/api/v1/store/bar/baz`

# Fetch existing key
- example:
    - `curl http://127.0.0.1/api/v1/store/foo`
    - `curl http://127.0.0.1/api/v1/store/bar/baz`
# Delete
- example:
    - `curl -X DELETE http://127.0.0.1/api/v1/store/foo`
    - `curl -X DELETE http://127.0.0.1/api/v1/store/bar/baz`